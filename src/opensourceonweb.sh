#!/bin/bash

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

exec 3>&1
exec 1>/dev/null

# shellcheck disable=SC2034
declare -A config

# shellcheck source=opensourceonweb.conf
if [[ -f "$SCRIPT_DIR/opensourceonweb.conf" ]]; then
	. "$SCRIPT_DIR/opensourceonweb.conf"
fi

# shellcheck source=/home/ash/.opensourceonweb.conf
if [[ -f "$HOME/.opensourceonweb.conf" ]]; then
	. "$HOME/.opensourceonweb.conf"
fi

declare -A RETVAL_ARR

print_r() {
	local -n arr=$1
	local -r arr_name=$1
	for key in "${!arr[@]}"; do
		printf "%s[\"%s\"]=\"%s\"\n" "$arr_name" "$key" "${arr[$key]}"
	done
}

get_scheme_delim_by_scheme["file"]=":///"
get_scheme_delim_by_scheme["https"]="://"
get_scheme_delim_by_scheme["http"]="://"
default_scheme_delim="://"

parse_url() {
	local -r url="$1"
	# https://tools.ietf.org:html/rfc3986#appendix-B
	local -r url_regex='^(([^:/?#]+)://)?(((([^:/?#]+)@)?([^:/?#]+)(:([^/]+))?))?(/([^?#]*))(\?([^#]*))?(#(.*))?'
	RETVAL_ARR=()

	[[ "$url" =~ $url_regex ]]
	#print_r BASH_REMATCH

	local -r detected_scheme=${BASH_REMATCH[2]}

	if [[ -n "$detected_scheme" ]]; then
		RETVAL_ARR["type"]="url"
		RETVAL_ARR["url"]="${BASH_REMATCH[0]}"
		RETVAL_ARR["scheme"]=${BASH_REMATCH[2]}
		RETVAL_ARR["password"]=${BASH_REMATCH[6]}
		RETVAL_ARR["host"]=${BASH_REMATCH[7]}
		RETVAL_ARR["port"]=${BASH_REMATCH[9]}
		RETVAL_ARR["path"]=${BASH_REMATCH[10]}
	else
		local -r host="[[:alnum:].-]+"
		local -r path=".*?"
		local -r git="\.git"
		local -r git_regex="^((git)@($host))(:($path)($git)?)?\$"

		[[ "$url" =~ $git_regex ]]
		#print_r BASH_REMATCH

		if [[ ${#BASH_REMATCH[@]} -gt 0 ]]; then
			RETVAL_ARR["type"]="git"
			RETVAL_ARR["url"]=${BASH_REMATCH[0]}
			RETVAL_ARR["base"]=${BASH_REMATCH[1]}
			RETVAL_ARR["username"]=${BASH_REMATCH[2]}
			RETVAL_ARR["host"]=${BASH_REMATCH[3]}
			RETVAL_ARR["path"]=${BASH_REMATCH[5]%%$git}
		else
			RETVAL_ARR["type"]="path"
			RETVAL_ARR["path"]=$url
		fi
	fi
	#print_r RETVAL_ARR
}

autodetect_git() {
	git status >/dev/null 2>&1
}

autodetect_svn() {
	svn info >/dev/null 2>&1
}

get_remote_url__git() {
	RETVAL=$(git config --get remote.origin.url)
}

get_remote_url__svn() {
	RETVAL=$(svn info | sed -n -e "s/^URL: \\(.*\\)$/\\1/p")
}

build_remote_url__git() {
	local -n server=$1

	local -r local_relative_fileordir="$2"
	local -r line_from="$3"
	local -r line_to="$4"

	local -r component=${server["path"]%/*}
	local -r module=${server["path"]##*/}

	local -r local_working_dir=$(pwd)
	local -r local_git_base_dir=$(git rev-parse --show-toplevel) # /home/ash/nic.cz/workspace/test/modules/server
	local -r local_relative_working_dir_=${local_working_dir#$local_git_base_dir}
	local -r local_relative_working_dir=${local_relative_working_dir_#/}

	local -r branch=$(git symbolic-ref -q HEAD --short || (git branch | sed -n -e "s/^.*(HEAD detached at \([^)]\+\)).*\$/\1/p"))

	local server_view="tree"
	if [[ -n "$local_relative_fileordir" ]]; then
		if [[ -d "$local_relative_fileordir" ]]; then
			local server_view="tree"
		elif [[ -f "$local_relative_fileordir" ]]; then
			local server_view="blob"
		else
			echo "Usage: ${0##*/} [file or directory]"
			echo "error: not file or directory" >&2
			exit
		fi
		local local_object="$local_relative_working_dir${local_relative_working_dir:+/}$local_relative_fileordir"
	else
		local local_object="$local_relative_working_dir"
	fi

	local -r alias=${config["${server["host"]}.alias"]:-${server["host"]}} # alias or host if alias not set
	local -r scheme=${config["$alias.scheme"]:-${server["scheme"]:-https}}
	local -r scheme_delim=${get_scheme_delim_by_scheme["$scheme"]:-$default_scheme_delim}
	local -r host=${config["$alias.host"]:-${server["host"]}}
	if [[ "$line_from" -eq 0 ]]; then
		local -r anchor=""
	elif [[ "$line_from" -eq "$line_to" ]]; then
		local -r anchor=${line_from:+#L$line_from}
	else
		local -r anchor=${line_from:+#L$line_from}${line_to:+-$line_to}
	fi

	# local -r url="$scheme$scheme_delim$host/$component/$module/$server_view/$commit" # TODO
	local -r url="$scheme$scheme_delim$host/$component/$module/$server_view/$branch/$local_object$anchor"

	RETVAL="$url"
}

build_remote_url__svn() {
	local -n server=$1
	local -r local_relative_fileordir="$2"
	local -r line_from="$3"
	# local -r line_to="$4" # not supported

	local -r p_regex="p"
	local -r svn_regex="svn"
	local -r trunk_regex="trunk"
	local -r component_regex="[^/]+"
	local -r relative_dir_regex=".*"

	[[ "${server["url"]}" =~ .*${server["host"]}[/:]$p_regex/($component_regex)/$svn_regex/$trunk_regex/?($relative_dir_regex) ]]
	print_r BASH_REMATCH
	local -r component=${BASH_REMATCH[1]}
	local -r relative_dir=${BASH_REMATCH[2]}

	local server_view="tree"
	if [[ -n "$local_relative_fileordir" ]]; then
		local local_object="$relative_dir${relative_dir:+/}$local_relative_fileordir"
	else
		local local_object="$relative_dir"
	fi

	local -r alias=${config["${server["host"]}.alias"]:-${server["host"]}} # alias or host if alias not set
	local -r scheme=${config["$alias.scheme"]:-${server["scheme"]}}
	local -r scheme_delim=${get_scheme_delim_by_scheme["$scheme"]:-$default_scheme_delim}
	local -r host=${config["$alias.host"]:-${server["host"]}}
	if [[ -z "${config["$alias.revision"]}" ]]; then
		local -r rev=$(svn info | sed -n -e "s/^Revision: \\(.*\\)$/\\1/p")
	elif [[ "${config["$alias.revision"]}" == "latest" ]]; then
		local -r rev
	else
		local -r rev="${config["$alias.revision"]#r}"
	fi
	if [[ "$line_from" -eq 0 ]]; then
		local -r anchor=""
	else
		local -r anchor=${line_from:+#l$line_from}
	fi

	local -r url="$scheme$scheme_delim$host/p/$component/svn/$rev${rev:+/$server_view/}trunk/$local_object$anchor"
	RETVAL="$url"
}

declare -A get_remote_url=( \
	[git]="get_remote_url__git" \
	[svn]="get_remote_url__svn"
)

declare -A build_remote_url=( \
	[git]="build_remote_url__git" \
	[svn]="build_remote_url__svn"
)

if autodetect_git; then
	vcs="git"
elif autodetect_svn; then
	vcs="svn"
else
	echo "error: VCS not detected" >&2
	exit 1
fi

printf "vcs: %s\\n" "$vcs"

${get_remote_url[$vcs]}
declare -r remote_url=$RETVAL

printf "remote url: %s\\n" "$remote_url"

if [[ -z "$remote_url" ]]; then
	echo "error: remote server URL not detected" >&2
	exit 1
fi

parse_url "$remote_url"

declare -A server_data
for i in "${!RETVAL_ARR[@]}"; do
	server_data[$i]="${RETVAL_ARR[$i]}"
done
print_r server_data

${build_remote_url[$vcs]} server_data "$@"
declare -r url="$RETVAL"

echo "$url" >&3
#firefox -remote "ping()" >/dev/null 2>&1 && firefox -remote "openURL($url,new-tab)" >/dev/null 2>&1 || firefox "$url" >/dev/null 2>&1
