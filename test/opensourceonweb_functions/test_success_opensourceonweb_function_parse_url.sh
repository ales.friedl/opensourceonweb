test_success_opensourceonweb_function_parse_url() {
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/opensourceonweb" </dev/null

	declare -A input_data
	input_data=(
		[gitlab1]="https://gitlab.com/ales.friedl/opensourceonweb"
		[gitlab2]="git@gitlab.com:ales.friedl/opensourceonweb.git"
		[svn]="https://svn.code.sf.net/p/whatever/svn/trunk/WhatEver/src"
	)
	output_data=(
		[gitlab1.protocol]="https"
		[gitlab1.login]=""
		[gitlab1.fqdn]="gitlab.com"
		[gitlab1.path]="ales.friedl/opensourceonweb"
		[gitlab2.protocol]=""
		[gitlab2.login]="git"
		[gitlab2.fqdn]="gitlab.com"
		[gitlab2.path]="ales.friedl/opensourceonweb.git"
		[svn.protocol]="https"
		[svn.login]=""
		[svn.fqdn]="svn.code.sf.net"
		[svn.path]="p/whatever/svn/trunk/WhatEver/src"
	)
	for value in "${!input_data[@]}"; do
		printf "Trying \"%s\"...\\n" "$value"
		parse_url "${input_data[$value]}"

		assert_equal "${RETVAL_ARR["url"]}" "${input_data[$value]}" || return 1
		for key in protocol login fqdn path; do
			assert_equal "${RETVAL_ARR[$key]}" "${output_data[$value.$key]}" || return 1
		done
	done
}
