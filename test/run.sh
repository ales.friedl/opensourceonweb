#!/bin/bash

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

# specifi path to project workspace
WORKSPACE="$SCRIPT_DIR/.."

PROJECT_ROOT="$WORKSPACE" \
BUNIT_TEST_DIR="$WORKSPACE/test" \
BUNIT_LOG_DIR="$WORKSPACE/log" \
BUNIT_COLORS="${BUNIT_COLORS:-true}" \
bash "$WORKSPACE/third_party/bunit/build/bin/bunit-runner"
